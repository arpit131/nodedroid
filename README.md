# (is-reachable) 

Steps to use

 * install [Termux](https://play.google.com/store/apps/details?id=com.termux) in android
 * Install node.js on android run these command on termux
 ```
 apt update && apt upgrade
 ```
 ```
 apt install coreutils
 ```
 ```
 apt install nodejs
 ```
 * Clone nodedroid repo and run 
 ```
 git clone https://bitbucket.org/nitingurbani/nodedroid/
 ```
 ```
 cd nodedroid
 ```
 ```
 npm install
 ```
 ```
 node index.js
 ```
 * to add more website add it in website.json
 * Termux tips (Ctrl+C = volume down + c and TAB = volume up + T)


### Pending stuff:

 * send report to slack or email or create json file
 

